#include<iostream>
#include"IInterogate.h"
#include"IParticipation.h"
#include"IMedals.h"
#include"IAthlete.h"
#include"Swimmer.h"
#include"Runner.h"
#include"TennisPlayer.h"

void PrintInterogation(const IInterogate* i)
{
	i->Interogate();
	std::cout << std::endl;
}

void PrintDisciplines(const IParticipation* p)
{
	int disciplines = p->Participation();
	if (disciplines & IParticipation::Swimm_50m)
		std::cout << "Swiming 50 meters" << std::endl;
	if (disciplines & IParticipation::Swimm_100m)
		std::cout << "Swiming 100 meters" << std::endl;
	if (disciplines & IParticipation::Running_50m)
		std::cout << "Running 50 meters" << std::endl;
	if (disciplines & IParticipation::Running_100m)
		std::cout << "Running 100 meters" << std::endl;
	if (disciplines & IParticipation::Tennis_singles)
		std::cout << "Tennis singles" << std::endl;
	if (disciplines & IParticipation::Tennis_doubles)
		std::cout << "Tennis doubles" << std::endl;
}
void PrintMedals(const IMedals* m)
{
	std::cout << "Medalii:" << m->NumMedals() << std::endl;
}

void PrintAthletes(const IAthlete** athletes, int numAthletes)
{
	for (int i = 0;i < numAthletes;i++)
	{
		const IInterogate* interogate = athletes[i]->AsInterogate();
		const IParticipation* participation= athletes[i]->AsParticipation();
		const IMedals* medals = athletes[i]->AsMedals();

		PrintInterogation(interogate);
		PrintDisciplines(participation);
		PrintMedals(medals);
		std::cout << "\n============================================================\n";
	}
}
void main()
{
	const IAthlete** a = new const IAthlete*[3];
	a[0] = new Swimmer("S W I M M E R", "Country1", 3, 5);
	a[1] = new Runner(" R U N N E R", "Country2", 12, 6);
	a[2] = new TennisPlayer("T E N N I S", "Country3", 32, 6);
	PrintAthletes(a, 3);
}